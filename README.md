# react-route-redux-boilerplate

## Usage
```
  yarn
  yarn start
  yarn build
```

## VSCode 
### settings
```json
{
    "window.zoomLevel": 0,
    "explorer.confirmDragAndDrop": false,
    "git.enableSmartCommit": true,
    "javascript.validate.enable": false,
    "editor.tabSize": 2
}
```

### Extentions
```
Flow Language Support
Reactjs code snippets
```

## Folder Sturcture
### index.js
```
entry point
```

### components
```
view
```
- common
  - To use on any pages or other components
- page
  - To use on a specific page
- templates
  - view template

### containers
```
logic
```

### lib
```
js libraries
```

### pages
```
composition of templates and containers
```

### router
```
routing
```

### store
```
redux
```
