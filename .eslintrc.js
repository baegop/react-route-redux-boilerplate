const path = require('path');

module.exports = {
  "extends": [
    "react-app",
    "airbnb"
  ],
  "settings": {
    "import/resolver": {
      node: { paths: [path.resolve('./src')] }
    },
  },
  "rules": {
    "no-unused-vars": "warn",
    "no-underscore-dangle": "off",
    "object-curly-newline": "off",
    "jsx-a11y/href-no-hash": "off",
    "react/jsx-filename-extension": "off"
  }
}