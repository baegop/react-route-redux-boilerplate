// @flow
import configure from './configure';
import type { Dummy } from './modules/dummy';

const store = configure();

export default store;

export type State = {
  dummy: Dummy,
  pender: {
    pending: any,
    success: any,
    failure: any
  }
};
