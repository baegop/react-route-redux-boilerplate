// @flow
import { createAction, handleActions } from 'redux-actions';
import { Record } from 'immutable';
// import { pender } from 'redux-pender';
// import * as AuthAPI from 'lib/api/auth';

const DUMMY = 'dummy/DUMMY';


export const actionCreators = {
  setDummyString: createAction(DUMMY, (value: string) => value),
};

export type DummyActionCreators = {
  setDummyString(value: string): any
}

export type Dummy = {
  dummyString: string
};

const DummyRecord = Record(({
  dummyString: '',
}: Dummy));

const initialState: Dummy = DummyRecord();

export default handleActions({
  [DUMMY]: (state, { payload: value }) => (
    state.set('dummyString', value)
  ),
  // ...pender({
  //   type: /* promiseAction */,
  //   onSuccess: (state, { payload: { data } }) => {
  //     const { arrvar, strvar } = data;
  //     return state.setIn(['arr1', 'arr2'], arrvar)
  //       .set('strvar', strvar);
  //   },
  // }),
}, initialState);
