// @flow
import { bindActionCreators } from 'redux';

import store from './index';
import { actionCreators as dummyActions, type DummyActionCreators } from './modules/dummy';

const { dispatch } = store;

export const DummyActions: DummyActionCreators = bindActionCreators(dummyActions, dispatch);

export default {};
