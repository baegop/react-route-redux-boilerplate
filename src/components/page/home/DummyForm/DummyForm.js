// @flow
import React from 'react';

import './DummyForm.scss';

type Props = {
  onChange(e: Event): void,
  dummyString: string,
};

const DummyForm = ({
  onChange,
  dummyString,
}: Props) => (
  <div className="dummy-form">
    <input
      value={dummyString}
      onChange={onChange}
    />
  </div>
);

export default DummyForm;
