// @flow
import React from 'react';

import './Header.scss';
import logo from './logo.svg';


const Header = () => (
  <header className="header">
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to react-route-redux-boilerplate</h1>
      </header>
    </div>
  </header>
);

export default Header;
