// @flow
import React from 'react';
import type { Node } from 'react';
import './HomeTemplate.scss';

type Prop = {
  form: Node,
};

const HomeTemplate = ({ form }: Prop) => (
  <div className="home-template">
    {form}
  </div>
);

export default HomeTemplate;
