import React from 'react';

import PageTemplate from 'components/templates/PageTemplate';
import HomeTemplate from 'components/templates/HomeTemplate';
import Header from 'components/common/Header';
import DummyContainer from 'containers/home/DummyContainer';

const Home = () => (
  <PageTemplate header={<Header />}>
    <HomeTemplate form={<DummyContainer />} />
  </PageTemplate>
);

export default Home;
