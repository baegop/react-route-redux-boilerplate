import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer as HotContainer } from 'react-hot-loader';

import router from 'router';
import store from 'store';

import registerServiceWorker from './registerServiceWorker';
import './index.css';

const render = (Component) => {
  ReactDOM.render(
    (
      <HotContainer>
        <Component store={store} />
      </HotContainer>
    ),
    document.getElementById('root'),
  );
};
render(router);

registerServiceWorker();
