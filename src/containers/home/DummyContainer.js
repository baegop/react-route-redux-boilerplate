// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DummyActions } from 'store/actionCreators';
import type { State } from 'store';
import DummyForm from 'components/page/home/DummyForm';

type Props = {
  something: string,
}

class DummyContainer extends Component<Props> {
  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const { value } = e.target;
    DummyActions.setDummyString(value);
  }

  render() {
    const { onChange } = this;
    const { something } = this.props;

    return (
      <div>
        <DummyForm
          dummyString={something}
          onChange={onChange}
        />
        <span>Input Text: {something}</span>
      </div>
    );
  }
}

export default connect(
  ({ dummy }: State) => ({
    something: dummy.dummyString,
  }),
  () => ({}),
)(DummyContainer);
